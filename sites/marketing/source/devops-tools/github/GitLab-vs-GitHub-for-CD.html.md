---
layout: markdown_page
title: "GitLab vs GitHub for CD"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.



<!------------------Begin page additions below this line ------------------ -->


## On this page
{:.no_toc}

- TOC
{:toc}

# GitLab CD Capabilities Missing in GitHub

| GitLab Capability                                                    | Features                                                                                                                                                                                                                   |
|----------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Built-in CD                                                          | No plug-ins or third party components.                                                                                                                                                                                     |
| Simplify DevOps process for new team members                         | AutoDevOps recognizes the code and automatically sets up the end to end devops template                                                                                                                                    |
| Model and manage large, complex projects and teams                   | Subgroups within Groups to manage large projects, Group File Templates (templates at the group level to drive standardization), Group drop-down in navigation to easily find groups,                                       |
| Preview App before Merge to reduce defects, shorten development time | Preview changes with review apps. Environments Autostop for review apps                                                                                                                                                    |
| Cloud Native development                                             | K8s Cluster Monitoring, Interactive Web Terminals (connect to K8s, Docker containers and run commands), View K8s pod logs, Deploy boards for environments running on K8s, Container debugging with integrated web terminal |                                                                                                          |

# GitHub CD Limitations

|                                   |                                                                                                                                                                                                                                                                                                                                                                                     |
|------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
| **Continuous Delivery**<br>Increase cost due to reduced reusability and limitation             | Cannot easily trigger different workflows for staging & prod environment<br>Lack of support for YAML anchors and aliases<br>Cannot trigger a new workflow from another workflow using the repository's GITHUB_TOKEN<br>Cannot trigger actions via Pull Request Messages/Comment<br>                                                                                         |
| **Enterprise Readiness**<br>Increased security risks, challenges in scalability/performance    | Manage secrets at a Team and Organization Level<br>GitHub Actions requires credentials for accessing Docker Images from a public repository.  Works manually through UI.<br>Making secrets available to builds of forks<br>GitHub Action support in GitHub Enterprise Server<br> |

# GitLab Top CD Differentiators

| GitLab Capability                                                                             | Features                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
|-----------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| GItLab CD does not require 3rd party Plugins/tools                                            | GitHub Actions requires integrating 3rd party plugins to <br>initiate the full CD process.  These 3rd party plugins can be costly to maintain and support.  The Plugin templates will also require a lot of time to  understand and edit for proper integration.  GitLab can support 3rd party plugins but they are not required.                                                                                                                                   |
| GitLab has built in Kubernetes Deployment and Monitoring                                      | Having the ability to easily add and monitor Kubernetes clusters within the GitLab UI provides a single touch point for you to deploy your apps to the cloud.  GitHub Actions Kubernetes integration varies with cluster type and may require configuration outside of the GitHub UI.                                                                                                                                                                               |
| GitLab offers Auto CI/CD Pipeline Configuration                                               | With GitLab Auto DevOps, you don’t have to go through the steps of configuring your CI/CD pipeline, it will be automatically set up based off of a set of predefined configurations and can be further altered to meet your needs .  GitHub Actions does not offer this, setting up your CI-CD pipeline will require you selecting Plugins.  Setting up your Kubernetes clusters with Actions also requires extra configuration steps to ensure proper integration. |
| GitLab offers built in CI-CD Security Scanning which does not require 3rd party Plugins/tools | GitHub can integrate Security scanning within the CI process but requires a third party tool (LGTM.com).  GitLab offers more security scans throughout the CICD pipeline which are available by default.  We can support 3rd party tools for security scanning but they are not necessary.                                                                                                                                                                          |
| GitLab’s UI provides a Security Dashboard enabling Security Team collaboration                | GitLab’s built in Security Dashboard promotes and improves synergy and collaboration within the Security group by providing them with a central view of CI-CD Security issues and also allowing them to communicate with each other on those issue right within the GitLab UI.  This CI-CD security team collaboration functions is not available with GitHub Actions.                                                                                              |

# GitLab vs GitHub CD Configuration Comparison

![GitLab GitHub CI Config Chart](/devops-tools/github/images/Gitlab-vs-GitHub-CD-Config.png)

# GitLab vs GitHub Kubernetes Configuration Comparison

![GitLab GitHub CI Config Chart](/devops-tools/github/images/GitLab-vs-GitHub-Kubernetes-Config.png)

# GitLab vs GitHub CI/CD Security Comparison

![GitLab GitHub CI Config Chart](/devops-tools/github/images/GitLab-vs-GitHub-CICDSecurity.png)

# GitLab vs GitHub CI/CD Summary Comparison

![GitLab GitHub CI Config Chart](/devops-tools/github/images/GitLab-vs-GItHub-CICD-Summary.png)
