- name: Ops Section - Section MAU - Sum of Ops section SMAU
  base_path: "/handbook/product/performance-indicators/"
  definition: A sum of all SMAUs across the five stages in the Ops Section (Verify,
    Package, Release, Configure & Monitor)
  target: 466k by End of Q3
  org: Ops Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  sisense_data:
    chart: 9082293
    dashboard: 634200
    embed: v2
  health:
    level: 3
    reasons:
    - Goal Attainment - On Track
    - Improvement - [Runner Guided Install](https://gitlab.com/groups/gitlab-org/-/epics/2778)
      and [CI Adoption Journey Research](https://gitlab.com/groups/gitlab-org/-/epics/4124)
    - Insight - SMAU Growth in Verify (23k from June to July) over doubles all other
      stages combined (~10k from June to July for Release, Monitor & Configure)
    - Insight - Higher growth and SMAU in Monitor over Configure. Likely due to instrumentation
      issues (or perhaps known cluster attach problems)
  instrumentation:
    level: 2
    reasons:
    - <a href="https://about.gitlab.com/handbook/product/ops-section-performance-indicators/index.html#packagepackage---smau---count-of-users-publishinginstalling-packages</a>  planned
    - Looking to improve our [Proxy Monitor and Configure SMAU](https://gitlab.com/gitlab-org/telemetry/-/issues/414)
  urls:
  - https://about.gitlab.com/direction/ops/#performance-indicators
  - https://about.gitlab.com/direction/ops/#user-adoption-journey
- name: Verify:CI & Verify:Runner - SMAU - Unique users triggering pipelines
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of unique users triggering pipelines in the last 28
    days
  target: Our FY21 Q3 Target is to see 10% growth. We are tracking towards a goal
    of 500k unique users triggering pipelines by end of FY21 (Jan 31, 2021).
  org: Ops Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  health:
    level: 2
    reasons:
    - Insight - While growth has been at 4% or greater on average since FY21 Q1, the
      growth rate has started to level but without known causes.
    - Improvement - We plan to assist users to author/edit their CI yaml files by
      providing a [graphical representation of their .gitlab-ci.yml file](https://gitlab.com/gitlab-org/gitlab/-/issues/15622).
    - Improvement - Because missing syntax in a .gitlab-ci.yml could prevent a pipeline
      from triggering, we will continue to deliver features to guide pipeline authoring, such as [warning
      messages to inform users of dependences that require the use of specific keywords
      in their CI yaml file](https://gitlab.com/gitlab-org/gitlab/-/issues/219431).
  instrumentation:
    level: 2
    reasons:
    - With ability to trigger a pipeline dependent on successful attempts to author
      CI yaml files, we have an issue to [track number of new .gitlab-ci.yml files
      created and time to first green build](https://gitlab.com/gitlab-org/gitlab/-/issues/232814)
    - We are also working to [instrument other performance indicators](https://gitlab.com/gitlab-data/analytics/-/issues/5319)
      for Verify:CI
  sisense_data:
    chart: 9090628
    dashboard: 634200
    embed: v2
- name: Verify:Testing - Paid GMAU - Count of active paid testing feature users
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of unique users who have interacted with a paid testing
    feature in the last 28 days
  target: TBD
  org: Ops Section
  public: true
  telemetry_type: SaaS
  is_primary: true
  is_key: false
  urls:
  - https://about.gitlab.com/handbook/engineering/development/ci-cd/verify/testing/#performance-indicators
  health:
    level: 0
    reasons:
    - Improvements - We are targeting delivery of [Group Code Coverage](https://gitlab.com/groups/gitlab-org/-/epics/2991)
      in 13.3 and 13.4.
  instrumentation:
    level: 2
    reasons:
    - Improvements - Implementation of page views for existing paid features [is in
      progress](https://gitlab.com/groups/gitlab-org/-/epics/2991).
    - Improvements - We have started collecting data for Code Quality Report Views
      in GitLab.com and will create a target after a month of data is collected.
  sisense_data:
    chart: 9397356
    dashboard: 633395
    embed: v2
- name: Package:Package - Other - Packages published/installed using the Package Registry
  base_path: "/handbook/product/performance-indicators/"
  definition: A count of events in which a package is published to or installed from
    the Package Registry on GitLab.com.
  target: The FY21 Q3 target 55k events per day, a 37% increase from Q2
  org: Ops Section
  public: true
  telemetry_type: SaaS
  is_primary: true
  is_key: false
  urls:
  - https://about.gitlab.com/handbook/engineering/development/ci-cd/package/#usage-funnels
  health:
    level: 3
    reasons:
    - Insight - PyPI adoption is growing faster in adoption than Composer.
    - Insight - Due to increased adoption of NPM, NuGet and PyPI, we have already hit our Q3 goal of 60k events per day.
    - Insight - We've also seen a 50% increase in page views to the Package Registry, likely the result of UX improvements. 
    - Improvements - In 13.4, we've made several improvements to the Conan Repository, we will do an impact analysis in 13.5.
    - Improvements - We intend to hit our Q3 target by moving the Package Registry
      to Core and by beginning to measure adoption for the new PyPI and Composer repositories.
  instrumentation:
    level: 2
    reasons:
    - We are currently tracking this data for GitLab.com via Snowplow.
    - In 13.4, we will begin to [measure user level adoption of the Package Registry with the Usage ping](https://gitlab.com/gitlab-org/gitlab/-/issues/205578)
    - Here is the [issue defining the PI](https://gitlab.com/gitlab-data/analytics/-/issues/4597)
  sisense_data:
    chart: 8104230
    dashboard: 527857
    embed: v2
- name: Package:Package - GMAU - Count of users publishing/installing packages
  base_path: "/handbook/product/performance-indicators/"
  definition: The maximum distinct count of users who either published a package or
    interacted with an installed package from the Package Registry. In the future,
    this value will the distinct count of users across all Package AMAUs.
  target: n/a
  org: Ops Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  urls:
  - https://about.gitlab.com/handbook/engineering/development/ci-cd/package/#aarrr-framework
  health:
    level: 1
    reasons:
    - Improvements - In 13.4, we intend to start measuring GMAU/SMAU for the Package
      stage via usage ping.
  instrumentation:
    level: 1
    reasons:
    - We have an issue open to [measure this data via the Usage ping](https://gitlab.com/gitlab-org/gitlab/-/issues/205578#note_368506625).
      It is currently scheduled for milestone 13.4.
- name: Package:Package - Paid GMAU - Count of paid users using the Dependency Proxy
  base_path: "/handbook/product/performance-indicators/"
  definition: The maximum distinct count of <a href="https://about.gitlab.com/handbook/product/performance-indicators/#paid-user">paidusers</a> who either published a package or interacted with an installed package from the Package Registry. In the future, this value will the distinct count of users across all Package AMAUs.
  target: n/a
  org: Ops Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  urls:
  - https://about.gitlab.com/handbook/engineering/development/ci-cd/package/#usage-funnels
  health:
    level: 1
    reasons:
    - Improvements - In 13.5, we intend to start measuring Paid GMAU for the Package
      stage via usage ping.
  instrumentation:
    level: 1
    reasons:
    - We have an issue open to [measure Dependency Proxy adoption via the Usage ping](https://gitlab.com/gitlab-org/gitlab/-/issues/238056). It is currently scheduled for milestone 13.5.
- name: Package:Package - AMAU - Count of users publishing packages
  base_path: "/handbook/product/performance-indicators/"
  definition: The distinct count of users who publish a package to the Package Registry.
  target: n/a
  org: Ops Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  urls:
  - https://about.gitlab.com/handbook/engineering/development/ci-cd/package/#usage-funnels
  health:
    level: 1
    reasons:
    - Improvements - In 13.4, we intend to start measuring GMAU/SMAU for the Package
      stage via usage ping.
  instrumentation:
    level: 1
    reasons:
    - In 13.4, we will begin to [measure user level adoption of the Package Registry with the Usage ping](https://gitlab.com/gitlab-org/gitlab/-/issues/205578).
- name: Package:Package - AMAU - Count of users installing packages
  base_path: "/handbook/product/performance-indicators/"
  definition: The distinct count of users who interact with a package that is installed
    from the Package Registry.
  target: n/a
  org: Ops Section
  public: true
  telemetry_type: Both
  is_primary: true
  is_key: false
  urls:
  - https://about.gitlab.com/handbook/engineering/development/ci-cd/package/#usage-funnels
  health:
    level: 1
    reasons:
    - Improvements - In 13.4, we intend to start measuring GMAU/SMAU for the Package
      stage via usage ping.
  instrumentation:
    level: 1
    reasons:
    - In 13.4, we will begin to [measure user level adoption of the Package Registry with the Usage ping](https://gitlab.com/gitlab-org/gitlab/-/issues/205578).
- name: Release - SMAU/GMAU - Count of users triggering deployments using GitLab
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of unique users triggering deployments with Gitlab in
    the last 28 days.
  target: Our FY21 Q3 Target is to see a growth of 24%.
  org: Ops Section
  public: true
  telemetry_type: both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Insight - We hit ~7% MoM for Count of Deployments, bringing in the growth for
      FY21 Q2 at 21%. We hope to hit this target again and slightly increase it in
      FY21 Q3.
    - Improvements - We saw a steeper increase in deployments from March to April,
      which could be attributed to the introduction of [Vault Managed App](https://about.gitlab.com/releases/2020/03/22/gitlab-12-9-released/#hashicorp-vault-gitlab-cicd-managed-application)
      and the [Group Deploy Tokens](https://about.gitlab.com/releases/2020/03/22/gitlab-12-9-released/#group-deploy-tokens).
      We see a steady increase from April to May, which can be attributed to the introduction
      of the [JWT authentication method to Vault](https://about.gitlab.com/releases/2020/04/22/gitlab-12-10-released/#retrieve-cicd-secrets-from-hashicorp-vault),
      [AWS Variables](https://about.gitlab.com/releases/2020/04/22/gitlab-12-10-released/#easy-to-configure-aws-deployment-variables),
      and enhanced [aseet links to releases](https://about.gitlab.com/releases/2020/04/22/gitlab-12-10-released/#link-runbooks-and-assets-to-a-release).
      Between May and we continue to see a standard ~2K increase in triggered deployments
      without introducing new deploy focused features.
    - Improvements - By delivering more at scale features in Release Management, to
      include sharing [releases](https://gitlab.com/groups/gitlab-org/-/epics/3561)
      and [environments](https://gitlab.com/gitlab-org/gitlab/-/issues/196168) at
      the group level we expect to positively influence the reach to users planning
      releases and deploying to many environments. On the Progressive Delivery front,
      continued investment in making it easy to [deploy to AWS](https://gitlab.com/gitlab-org/gitlab/-/issues/196049)
      will reduce friction to deploying to AWS as target.
  instrumentation:
    level: 3
    reasons:
    - We are currently tracking this data for GitLab.com via Snowplow and as a usage
      ping for self-managed instances.
    - We need to [Expand Revenue Attribution to Release SMAU](https://gitlab.com/gitlab-data/analytics/-/issues/4461)
    - Improvements - Counter was instrumented for 28-days
    - Improvements - User and unique count of deployments are instrumented
    - Improvements - SaaS and self-managed data are both available on a single chart
  sisense_data:
    chart: 8578386
    dashboard: 661492
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/640196/New-WIP-GitLab.com-+-Self-Managed-Usage-Ping-SMAU
  - https://app.periscopedata.com/app/gitlab/661492/Temp---Eli-Release-Stage-Examples?widget=8578558&udv=0
- name: Release:Release Management - AMAU - Unique user count viewing Release and
    Environment pages
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of environment page and release page view for Gitlab.com
    in the last 28 days.
  target: Our FY21 Q3 Target is to see a growth of 10%.
  org: Ops Section
  public: true
  telemetry_type: both
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Improvements - Counter was instrumented for 28-days
    - Improvements - Unique counts of views for both Release Page and Environments
      Page
    - Improvements - SaaS and self-managed data are both available on a single chart
  instrumentation:
    level: 2
    reasons:
    - We are currently tracking this data for GitLab.com via Snowplow
  sisense_data:
    chart: 7001781
    dashboard: 540095
    embed: v2
- name: Release:Progressive Delivery:Feature Flags - AMAU - Unique users triggering
    feature flag toggles
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of users triggering feature flag toggles with Gitlab
    in the last 28 days.
  target: TBD
  org: Ops Section
  public: true
  telemetry_type: both
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - No target set
  instrumentation:
    level: 0
    reasons:
    - Improvements - Create the instrumentation [Count UI Feature Flag Toggles - Snowplow](https://gitlab.com/gitlab-org/gitlab/-/issues/31970)
    - Improvements - Create the instrumentation [Count API Feature Flag Toggles -
      Snowplow](https://gitlab.com/gitlab-org/gitlab/-/issues/233872)
    - Improvements - Add Usage Ping data
- name: Release:Progressive Delivery:Continuous Delivery - AMAU - Unique users triggering
    pipelines with automatic deployments (no manual jobs)
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of users triggering automatic deployments (in without
    any manual activity) with Gitlab in the last 28 days.
  target: TBD
  org: Ops Section
  public: true
  telemetry_type: both
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - No target set
  instrumentation:
    level: 0
    reasons:
    - Improvements - Create the instrumentation [Instrument AMAU of CD](https://gitlab.com/gitlab-org/gitlab/-/issues/233928)
- name: Release:Progressive Delivery:Review Apps - AMAU - Unique users triggering
    pipelines that created a review app
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of unique users triggering pipelines which created a
    review app with Gitlab in the last 28 days.
  target: TBD
  org: Ops Section
  public: true
  telemetry_type: both
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - No target set
  instrumentation:
    level: 0
    reasons:
    - Improvements - Revive the instrumentation
- name: Release:Progressive Delivery:Advanced Deployments - AMAU - Count of unique
    users using Canary or Incremental Rollout
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of users using advanced deployments, namely, canary
    and incremental rollouts with Gitlab in the last 28 days.
  target: TBD
  org: Ops Section
  public: true
  telemetry_type: both
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - No target set
  instrumentation:
    level: 0
    reasons:
    - Improvements - Create the instrumentation [Instrument AMAU of Canary Rollouts](https://gitlab.com/gitlab-org/gitlab/-/issues/233899)
    - Improvements - Create the instrumentation [Instrument AMAU of Incremental Rollouts](https://gitlab.com/gitlab-org/gitlab/-/issues/233903)
- name: Release:Progressive Delivery:Deployment to AWS - AMAU - Count of users using
    AutoDevOps or Ci templates to deploy to AWS
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of users using using AutoDevOps or Ci templates to deploy
    to AWS based on  cloud_service:$CI_CLUSTER_CLOUD_SERVICE_TYPE in the last 28 days.
  target: TBD
  org: Ops Section
  public: true
  telemetry_type: both
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - No target set
  instrumentation:
    level: 0
    reasons:
    - Improvements - Create the instrumentation [New db model for identifying the
      deployment target](https://gitlab.com/gitlab-org/gitlab/-/issues/217707)
- name: Configure - SMAU - Proxy user count based on attached clusters
  base_path: "/handbook/product/performance-indicators/"
  definition: A count of all users in projects that have an attached cluster because
    project users receive benefit from the cluster without direct usage.
  target: 5k Configure SMAU by the end of Q3FY21 (20% growth)
  org: Ops Section
  public: true
  telemetry_type: Both
  is_primary: false
  is_key: false
  health:
    level: 2
    reasons:
    - No target defined before Q3FY21
  instrumentation:
    level: 2
    reasons:
    - Proxy SMAU
  sisense_data:
    chart: 9090632
    dashboard: 634200
    embed: v2
- name: Configure:Configure - Other - Number of CI/CD pipeline runs going through
    our infrastructure management deployment oriented features
  base_path: "/handbook/product/performance-indicators/"
  definition: A rolling count of pipelines calling infrastructure management deployment
    commands in the last 28 days
  target: TBD
  org: Ops Section
  public: true
  telemetry_type: SaaS
  is_primary: true
  is_key: false
  health:
    level: 0
    reasons:
    - No target defined yet
    - Auto DevOps - need to figure out [a way to measure templates usage](https://gitlab.com/gitlab-org/gitlab/-/issues/219457)
    - Insight - IaC - adoption showed up really quickly. 2.7% and 2.5% of premium
      and gold namespaces started to use Terraform features on gitlab.com. We see
      active usage of terraform reports generated (8000+ by silver users). bronze
      and free users use our features much less.
    - Kubernetes - had a design sprint, working on fixing the biggest shortcomings
      to increase adoption, currently [working to ship new approach](https://gitlab.com/gitlab-org/gitlab/-/issues/220868)
  instrumentation:
    level: 2
    reasons:
    - We're still missing reliable metrics for self-managed instances.
    - Improving chart to [single metric across all infrastructure deployment types](https://gitlab.com/gitlab-data/analytics/-/issues/4638)
    - Insight - With our IaC developments, we've started to collect reliable, usage-oriented
      metrics
    - Improvement - we want to start collecting activity-based data with the GitLab
      Kubernetes Agent to be released soon
    - Improvement - change the SMAU metrics with https://gitlab.com/gitlab-org/growth/product/-/issues/1612
  sisense_data:
    chart: 7833255
    dashboard: 511813
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/511813/Configure-team-business-metrics
- name: Monitor - SMAU - Proxy user count based on prometheus integration
  base_path: "/handbook/product/performance-indicators/"
  definition: A count of all users in projects that have a connected Prometheus integration
    because project users receive benefit from the cluster without direct usage.
  target: 11k Monitor SMAU by the end of Q3FY21 (20% growth)
  org: Ops Section
  public: true
  telemetry_type: Both
  is_primary: false
  is_key: false
  health:
    level: 2
    reasons:
    - No target defined prior to Q3FY21
  instrumentation:
    level: 2
    reasons:
    - Proxy SMAU
  sisense_data:
    chart: 9090633
    dashboard: 634200
    embed: v2
- name: Monitor:APM - GMAU - Total unique users that view metrics or logs
  base_path: "/handbook/product/performance-indicators/"
  definition: Total number of unique users that view metrics and log on GitLab.com
  target:
  org: Ops Section
  public: true
  telemetry_type: SaaS
  is_primary: true
  is_key: false
  health:
    level: 3
    reasons:
    - Improvments - We recently moved to measure GMAU, Q3 goal needs to be defined
    - Improvments - [Add metric to a custom dashboard workflow](https://gitlab.com/groups/gitlab-org/-/epics/2882)
    - Improvments - [Allow developers to view K8s logs](https://gitlab.com/gitlab-org/gitlab/-/issues/218861)
    - Insights - Logging views looks stable and seems to be the leading indicator
      for the last 3 months
    - Insights - Over the last 3 month there seems to be a reverse correlation between
      the default and custom dashboard
    - Insights - June 2020 APM full telemetry analysis can be found [here](https://gitlab.com/gitlab-org/monitor/apm/-/issues/47)
  instrumentation:
    level: 2
    reasons:
    - Will update to 7 day rolling average
    - Does not include Self-Managed via Usage Ping
  sisense_data:
    chart: 9033545
    dashboard: 636549
    embed: v2
  url:
  - https://app.periscopedata.com/app/gitlab/636549/APM---CUSTOMER-USAGE-(Logs-+-Metrics)
  - https://about.gitlab.com/handbook/engineering/development/ops/monitor/apm/#north-star-metric-nsm
- name: Monitor:Health - Other - Count of projects where incidents are being created
  base_path: "/handbook/product/performance-indicators/"
  definition: Count of projects where incidents are being created
  target: 100 projects where incidents are being created for GitLab.com by the end
    of Q3 - October 2020
  org: Ops Section
  public: true
  telemetry_type: SaaS
  is_primary: true
  is_key: false
  health:
    level: 1
    reasons:
    - Improvements - Tracking the number of incidents was not the best indicator of
      growth - the number of incidents at a single company can significantly vary
      month to month based on many factors, such as the type of services they provide,
      the time of year (impacts user traffic), the stability of vendor products they
      depend on, culture of the Operations team, etc. We are seeing a month over month
      increase in [users creating incidents](https://app.periscopedata.com/app/gitlab/668012/Health-Group-Dashboard?widget=9382779&udv=0)
      and [projects where incidents are being created](https://app.periscopedata.com/app/gitlab/668012/Health-Group-Dashboard?widget=9382800&udv=0),
      which does not mirror the same trend in the number of incidents being created.
      We are going to keep this metric and continue to observe it over the next few
      milestones. The Health Group PI has been updated to **the number of projects
      where incidents are being created**.
    - Insights - We are below our current target because the incident management product
      is new and this is the first time we've set a target.
    - Insights - We are seeing a month over month increase in the [number of users
      creating incidents](https://app.periscopedata.com/app/gitlab/668012/Health-Group-Dashboard?widget=9382779&udv=0)
      and the number of [projects where incidents are being created](https://app.periscopedata.com/app/gitlab/668012/Health-Group-Dashboard?widget=9382800&udv=0)
      from December 2019 to present. We can surmise this is because we've enabled
      the end-to-end triage workflow fully in GitLab during that time.
    - Improvements - [Health Group growth model](https://about.gitlab.com/handbook/engineering/development/ops/monitor/health/#north-star-metric)
      was updated to reflect change in PI and insights above - [MR for change](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/58739)
    - Improvements - Health Group is focused on [table-stakes](https://brandmarketingblog.com/articles/branding-definitions/table-stakes-business/)
      features to build user base. [This epic](https://gitlab.com/groups/gitlab-org/-/epics/1494)
      contains the plan for the next few milestones.
  instrumentation:
    level: 2
    reasons:
    - Data is currently being captured for GitLab.com
    - Improvements - We need to instrument and dashboard data for the usage ping -
      work for this is being tracked [here](https://gitlab.com/gitlab-org/gitlab/-/issues/233719).
    - Improvements - We need a single chart that overlays GitLab.com and usage ping
      data so that we have one chart for the group PI. Work to complete this is being
      tracked in this [issue](https://gitlab.com/gitlab-data/analytics/-/issues/5099).
    - Improvements - Additional metrics for the [Health Group growth model](https://about.gitlab.com/handbook/engineering/development/ops/monitor/health/#north-star-metric)
      are being instrumented in [this issue](https://gitlab.com/groups/gitlab-org/-/epics/4137).
  sisense_data:
    chart: 9382800
    dashboard: 668012
    embed: v2
  urls:
  - https://app.periscopedata.com/app/gitlab/668012/Health-Group-Dashboard
  - https://about.gitlab.com/handbook/engineering/development/ops/monitor/health/#north-star-metric
- name: Monitor:Health - GMAU - Unique users that interact with Alerts and Incidents
  base_path: "/handbook/product/performance-indicators/"
  definition: TBD
  target: TBD
  org: Ops Section
  public: true
  telemetry_type: both
  is_primary: false
  is_key: false
  urls:
  - https://about.gitlab.com/handbook/engineering/development/ops/monitor/health/#north-star-metric
  health:
    level: 0
    reasons:
    - No target set
  instrumentation:
    level: 0
    reasons:
    - TBD
